type _pushEvent<'a, 'data>;
type pushEvent_like<'a, 'data> =
  Notifications.ExtendableEvent.t_like<_pushEvent<'a, 'data>>;
type t<'data> = pushEvent_like<Dom._baseClass, 'data>;

@send external makeWithOptions: (string, EventInitDict.t<'data>) => t<'data>
  = "PushEvent";

@send external makeWithoutOptions: string => t<'data> = "PushEvent";

@get external data: t<'data> => option<PushMessageData.t<'data>> = "data";
