/* Note: I have no plans to implement deprecated methods and properties, but
 * if you need them, by all means open an issue and submit a pull request. I'm
 * open to adding them if there's a need. */

@scope(("PushManager")) @val
external
supportedContentEncodings: unit => array<string> = "supportedContentEncodings";

type t;

/* methods */
@send external getSubscription: t => promise<PushSubscription.t>
  = "getSubscription";

@send
external permissionState: t => promise<string> = "permissionState";

@send
external subscribe: t => promise<PushSubscription.t> = "subscribe";
