type t<'data>;

@send external arrayBuffer: t<'data> => RescriptCore.ArrayBuffer.t
  = "arrayBuffer";

// TODO: What do I do with `blob`?

@send external json: t<'data> => 'data = "json";

@send external text: t<'data> => string = "text";
