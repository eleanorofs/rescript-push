type t;

/* properties */
@get external endpoint: t => string = "endpoint";

@get
external expirationTime: t => option<int> = "expirationTime";

@get external options: t => SubscriptionOptions.t = "options";

/* methods */
@send
external getKey: (t, string) => RescriptCore.ArrayBuffer.t = "getKey";

@send external toJson: t => string = "toJson";

@send external unsubscribe: t => option<bool> = "unsubscribe";
