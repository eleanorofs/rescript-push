# rescript-push

This package closely wraps the
[Push API](https://developer.mozilla.org/en-US/docs/Web/API/Push_API). 
It does not include the Push API Service Worker Additions. Instead,
it is a dependency of the `rescript-service-worker` package. It has not been
exhaustively tested. 

## Long-term support

This package is currently supported but may go long periods of time without
fresh commits because it is something like "finished". Yes, software is like 
fashion, and fashion is never finished, and I will try to upgrade the ReScript 
language version from time to time, but unless there are significant defects 
in this project you can generally consider this project to be as stable as the 
ReScript language and the Push API. 

This is to say: Don't be alarmed by the lack of recent commits! Stability is
good! And don't be afraid to open issues. 

## Installation
`npm i rescript-push`

## Implemented
- [X] [EventInitDict](https://developer.mozilla.org/en-US/docs/Web/API/PushEvent/PushEvent)
- [X] [EventType](https://developer.mozilla.org/en-US/docs/Web/API/PushEvent/PushEvent)
- [X] [ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent)
- [X] [PushEvent](https://developer.mozilla.org/en-US/docs/Web/API/PushEvent/PushEvent)
- [X] [PushManager](https://developer.mozilla.org/en-US/docs/Web/API/PushManager)
- [X] [PushManagerPermissionState](https://developer.mozilla.org/en-US/docs/Web/API/PushManager/permissionState)
- [X] [PushMessageData](https://developer.mozilla.org/en-US/docs/Web/API/PushMessageData)
- [X] [PushSubscription](https://developer.mozilla.org/en-US/docs/Web/API/PushSubscription)
- [X] [SubscriptionOptions](https://developer.mozilla.org/en-US/docs/Web/API/PushSubscription/options)

## Notes
### 'data
`'data` is a frequent type parameter. It represents the untyped data object
passed into the eventInitDict of the PushEvent constructor. 

### PushMessageData
There's currently no ReScript `Blob` type binding that I'm aware of, so
rather than find a workaround, I'm just leaving that one as an exercise for 
the reader. 

## License

Because I have merely translated the API from the JavaScript specification
into ReScript with no implementation, I do not consider anything in this
repository to be copyrightable. 

As a courtesy, I have included a copy of the MIT license anyway just in case 
your lawyers are fussy about this sort of thing. If you'd rather have a 
different license instead or in addition to it, let me know and I'll write one
for you. 
